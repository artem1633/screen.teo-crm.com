<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "history".
 *
 * @property int $id
 * @property string $table_name Таблица
 * @property int $line_id ИД
 * @property string $date_time Дата и время
 * @property int $user_id Пользователь
 * @property string $user_fio ФИО пользователя
 * @property string $field Поля
 * @property string $old_value Значение
 * @property string $new_value Изменение
 *
 * @property Users $user
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['line_id', 'user_id'], 'integer'],
            [['date_time'], 'safe'],
            [['old_value', 'new_value'], 'string'],
            [['table_name', 'user_fio', 'field'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table_name' => 'Таблица',
            'line_id' => 'ИД',
            'date_time' => 'Дата и время',
            'user_id' => 'Пользователь',
            'user_fio' => 'ФИО пользователя',
            'field' => 'Поля',
            'old_value' => 'Значение',
            'new_value' => 'Изменение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public function setValues($table_name, $line_id, $user_id, $user_fio, $field, $old_value, $new_value)
    {
        $history = new History();
        $history->table_name = (string)$table_name;
        $history->line_id = $line_id;
        $history->date_time = date('Y-m-d H:i:s');
        $history->user_id = $user_id;
        $history->user_fio = (string)$user_fio;
        $history->field = (string)$field;
        $history->old_value = (string)$old_value;
        $history->new_value = (string)$new_value;
        $history->save();
    }

    public function search($params, $post, $table_name, $line_id)
    {
        $query = History::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($post['date_time_from'] !== null && $post['date_time_to'] !== null) $query->andFilterWhere(['between', 'date_time', date('Y-m-d 00:00:00', strtotime($post['date_time_from'] ) ),  date('Y-m-d 23:59:59', strtotime($post['date_time_to'] )) ]);

        if($post['field'] !== null) $query->andFilterWhere(['field' => $post['field']]);

        $query->andFilterWhere([
            'id' => $this->id,
            'line_id' => $this->line_id,
            'date_time' => $this->date_time,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'table_name', $this->table_name])
            ->andFilterWhere(['like', 'user_fio', $this->user_fio])
            ->andFilterWhere(['like', 'field', $this->field])
            ->andFilterWhere(['like', 'old_value', $this->old_value])
            ->andFilterWhere(['like', 'new_value', $this->new_value]);

        return $dataProvider;
    }
}
