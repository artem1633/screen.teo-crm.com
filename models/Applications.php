<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "applications".
 *
 * @property int $id
 * @property string $date_cr Дата и время создание
 * @property int $city_id Город
 * @property int $district_id Район
 * @property int $street_id Улица
 * @property int $house_id Дом
 * @property string $kvartira Квартира
 * @property string $l_s Л\С
 * @property string $fio ФИО
 * @property string $phone Телефон
 * @property string $description Описание
 * @property int $status Статус
 * @property int $creator_id Создатель
 * @property string $comment Комментария
 * @property string $date_end Дата завершение
 * @property int $executor_id Исполнитель
 * @property string $date_execute Дата исполнения
 * @property string $comment_execute Отчет выполнения
 *
 * @property City $city
 * @property Users $creator
 * @property District $district
 * @property Users $executor
 * @property House $house
 * @property ApplicationStatus $status0
 * @property Street $street
 */
class Applications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'applications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_cr', 'date_end', 'date_execute'], 'safe'],
            [['city_id', 'district_id', 'street_id', 'house_id'], 'required'],
            [['city_id', 'district_id', 'street_id', 'house_id', 'status', 'creator_id', 'executor_id', 'type'], 'integer'],
            [['description', 'comment', 'comment_execute'], 'string'],
            [['kvartira', 'l_s', 'fio', 'phone'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['creator_id' => 'id']],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => District::className(), 'targetAttribute' => ['district_id' => 'id']],
            [['executor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['executor_id' => 'id']],
            [['house_id'], 'exist', 'skipOnError' => true, 'targetClass' => House::className(), 'targetAttribute' => ['house_id' => 'id']],
            [['street_id'], 'exist', 'skipOnError' => true, 'targetClass' => Street::className(), 'targetAttribute' => ['street_id' => 'id']],
            [['status'], 'exist', 'skipOnError' => true, 'targetClass' => ApplicationStatus::className(), 'targetAttribute' => ['status' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_cr' => 'Дата создание',
            'city_id' => 'Город',
            'district_id' => 'Район',
            'street_id' => 'Улица',
            'house_id' => 'Дом',
            'kvartira' => 'Квартира',
            'l_s' => 'Л\\С',
            'fio' => 'ФИО',
            'phone' => 'Телефон',
            'description' => 'Описание',
            'status' => 'Статус',
            'creator_id' => 'Создатель',
            'comment' => 'Комментария',
            'date_end' => 'Дата завершение',//kk emas
            'executor_id' => 'Исполнитель',
            'date_execute' => 'Дата выполнения',
            'comment_execute' => 'Выполненные работы',
            'type' => 'Тип',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(Users::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(District::className(), ['id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutor()
    {
        return $this->hasOne(Users::className(), ['id' => 'executor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHouse()
    {
        return $this->hasOne(House::className(), ['id' => 'house_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(ApplicationStatus::className(), ['id' => 'status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStreet()
    {
        return $this->hasOne(Street::className(), ['id' => 'street_id']);
    }


    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_cr = date('Y-m-d H:i:s');
            $this->creator_id = Yii::$app->user->identity->id;
        }
        return parent::beforeSave($insert);
    }

    public function getTypeList()
    {
        return [
            1 => 'Обслуживание',
            2 => 'Подключение',
        ];
    }
    
    public function getStatusList()
    {
        $status = ApplicationStatus::find()->all();
        return ArrayHelper::map($status, 'id', 'name');
    }

    public function getCreatorList()
    {
        $user = Users::find()->all();
        return ArrayHelper::map($user, 'id', 'name');
    }

    public function getExecutorList()
    {
        $user = Users::find()->where(['role_id' => 3])->all();
        return ArrayHelper::map($user, 'id', 'name');
    }

    public function getCityList()
    {
        $city = City::find()->all();
        return ArrayHelper::map($city, 'id', 'name');
    }

    public function getDistrictList($city_id = null, $type = 0)
    {
        if($type == 0) $dist = District::find()->all();
        else $dist = District::find()->where(['city_id' => $city_id])->all();
        return ArrayHelper::map($dist, 'id', 'name');
    }

    public function getStreetList($district_id = null, $type = 0)
    {
        if($type == 0) $street = Street::find()->all();
        else $street = Street::find()->where(['district_id' => $district_id])->all();
        return ArrayHelper::map($street, 'id', 'name');
    }

    public function getHouseList($street_id = null, $type = 0)
    {
        if($type == 0) $house = House::find()->all();
        else $house = House::find()->where(['street_id' => $street_id])->all();
        return ArrayHelper::map($house, 'id', 'name');
    }

    public function getFieldList()
    {
        return [
            'Город' => 'Город',
            'Район' => 'Район',
            'Улица' => 'Улица',
            'Дом' => 'Дом',
            'Размер' => 'Размер',
            'Квартира' => 'Квартира',
            'Л\\С' => 'Л\\С',
            'ФИО' => 'ФИО',
            'Телефон' => 'Телефон',
            'Описание' => 'Описание',
            'Статус' => 'Статус',
            'Тип' => 'Тип',
            'Комментария' => 'Комментария',
            'Дата завершение' => 'Дата завершение',
            'Исполнитель' => 'Исполнитель',
            'Дата выполнения' => 'Дата выполнения',
            'Выполненные работы' => 'Выполненные работы',
        ];
    }

    public function setHistory($old, $new)
    {
        $user_id = Yii::$app->user->identity->id;
        $user_name = Yii::$app->user->identity->name;

        if($old->city_id != $new->city_id) History::setValues('applications', $new->id, $user_id, $user_name, 'Город', $old->city->name, $new->city->name);
        if($old->district_id != $new->district_id) History::setValues('applications', $new->id, $user_id, $user_name, 'Район', $old->district->name, $new->district->name);
        if($old->street_id != $new->street_id) History::setValues('applications', $new->id, $user_id, $user_name, 'Улица', $old->street->name, $new->street->name);
        if($old->house_id != $new->house_id) History::setValues('applications', $new->id, $user_id, $user_name, 'Дом', $old->house->name, $new->house->name);
        if($old->kvartira != $new->kvartira) History::setValues('applications', $new->id, $user_id, $user_name, 'Квартира', $old->kvartira, $new->kvartira);
        if($old->l_s != $new->l_s) History::setValues('applications', $new->id, $user_id, $user_name, 'Л\\С', $old->l_s, $new->l_s);
        if($old->fio != $new->fio) History::setValues('applications', $new->id, $user_id, $user_name, 'ФИО', $old->fio, $new->fio);
        if($old->phone != $new->phone) History::setValues('applications', $new->id, $user_id, $user_name, 'Телефон', $old->phone, $new->phone);
        if($old->description != $new->description) History::setValues('applications', $new->id, $user_id, $user_name, 'Описание', $old->description, $new->description);
        if($old->status != $new->status) History::setValues('applications', $new->id, $user_id, $user_name, 'Статус', $old->status0->name, $new->status0->name);
        if($old->comment != $new->comment) History::setValues('applications', $new->id, $user_id, $user_name, 'Комментария', $old->comment, $new->comment);
        if($old->date_end != $new->date_end) History::setValues('applications', $new->id, $user_id, $user_name, 'Дата завершение', $old->date_end, $new->date_end);
        if($old->date_execute != $new->date_execute) History::setValues('applications', $new->id, $user_id, $user_name, 'Дата выполнения', $old->date_execute, $new->date_execute);
        if($old->comment_execute != $new->comment_execute) History::setValues('applications', $new->id, $user_id, $user_name, 'Выполненные работы', $old->comment_execute, $new->comment_execute);
        if($old->executor_id != $new->executor_id) History::setValues('applications', $new->id, $user_id, $user_name, 'Исполнитель', $old->executor->name, $new->executor->name);
        if($old->type != $new->type) History::setValues('applications', $new->id, $user_id, $user_name, 'Тип', $old->getTypeList()[$old->type], $new->getTypeList()[$new->type]);
    }

    public function getDisabled($field)
    {
        $role_id = Yii::$app->user->identity->role_id;
        if($this->isNewRecord) return false;
        else{
            if($field == 'comment' || $field == 'status' || $field == 'executor_id' || $field == 'date_execute' || $field == 'comment_execute') return false;
            if($field == 'city_id' || $field == 'district_id' || $field == 'street_id' || $field == 'house_id' || $field == 'kvartira' || $field == 'l_s' || $field == 'fio' || $field == 'phone' || $field == 'type' || $field == 'description'){
                if($role_id == 3) return true;
                else return false;                
            }
            return true;
        }
    }
}
