<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $name
 * @property string $login
 * @property string $password
 * @property string $telephone
 * @property string $role_id
 * @property string $auth_key
 */
class Users extends \yii\db\ActiveRecord
{
    public $new_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'password', 'login'], 'required'],
            [['role_id'], 'integer'],
            //[['login'], 'email'],
            [['password', 'new_password'], 'string', 'max' => 255, 'min' => 6],
            [['name', 'login', 'telephone', 'role_id', 'auth_key'], 'string', 'max' => 255],
            [['login'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'login' => 'Логин',
            'password' => 'Пароль',
            'telephone' => 'Телефон',
            'role_id' => 'Должность',
            'auth_key' => 'Пароль',
            'new_password' => 'Новый пароль',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->auth_key = $this->password;
            $this->password = md5($this->password);
        }

        if($this->new_password != null) 
        {
            $this->auth_key = $this->new_password;
            $this->password = md5($this->new_password);
        }
        return parent::beforeSave($insert);
    }

    public function getRoleList()
    {
        return ArrayHelper::map([
            ['id' => 1, 'name' => 'Администратор',],
            ['id' => 2, 'name' => 'Оператор',],
            ['id' => 3, 'name' => 'Исполнитель',],
        ], 'id', 'name');
    }

    public function getRoleDescription()
    {        
        if(1 == $this->role_id) return 'Администратор';
        if(2 == $this->role_id) return 'Оператор';
        if(3 == $this->role_id) return 'Исполнитель';        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Applications::className(), ['creator_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications0()
    {
        return $this->hasMany(Applications::className(), ['executor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistories()
    {
        return $this->hasMany(History::className(), ['user_id' => 'id']);
    }

}
