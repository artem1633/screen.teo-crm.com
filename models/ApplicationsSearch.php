<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Applications;

/**
 * ApplicationsSearch represents the model behind the search form about `app\models\Applications`.
 */
class ApplicationsSearch extends Applications
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'district_id', 'street_id', 'house_id', 'status', 'creator_id', 'executor_id', 'type'], 'integer'],
            [['date_cr', 'kvartira', 'l_s', 'fio', 'phone', 'description', 'comment', 'date_end', 'date_execute', 'comment_execute'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Applications::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'district_id' => $this->district_id,
            'street_id' => $this->street_id,
            'house_id' => $this->house_id,
            'status' => $this->status,
            'creator_id' => $this->creator_id,
            'date_end' => $this->date_end,
            'executor_id' => $this->executor_id,
            'type' => $this->type,
            'date_execute' => $this->date_execute,
        ]);

        $query->andFilterWhere(['like', 'kvartira', $this->kvartira])
            ->andFilterWhere(['like', 'l_s', $this->l_s])
            ->andFilterWhere(['like', 'comment_execute', $this->comment_execute])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        if(isset($params['ApplicationsSearch']['date_cr'])) {
            $array = explode(" - ", $params['ApplicationsSearch']['date_cr']);
            if(count($array) > 0) $query->andFilterWhere(['between', 'date_cr', $array[0], $array[1]]);
        }

        return $dataProvider;
    }
}
