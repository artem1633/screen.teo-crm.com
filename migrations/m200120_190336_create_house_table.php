<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%house}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%street}}`
 */
class m200120_190336_create_house_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%house}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'street_id' => $this->integer()->comment('Улица'),
        ]);

        // creates index for column `street_id`
        $this->createIndex(
            '{{%idx-house-street_id}}',
            '{{%house}}',
            'street_id'
        );

        // add foreign key for table `{{%street}}`
        $this->addForeignKey(
            '{{%fk-house-street_id}}',
            '{{%house}}',
            'street_id',
            '{{%street}}',
            'id',
            'CASCADE'
        );

        $this->insert('house',array(
            'name' => '70',
            'street_id' => 1,
        ));

        $this->insert('house',array(
            'name' => '45',
            'street_id' => 2,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%street}}`
        $this->dropForeignKey(
            '{{%fk-house-street_id}}',
            '{{%house}}'
        );

        // drops index for column `street_id`
        $this->dropIndex(
            '{{%idx-house-street_id}}',
            '{{%house}}'
        );

        $this->dropTable('{{%house}}');
    }
}
