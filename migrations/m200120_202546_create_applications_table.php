<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%applications}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%city}}`
 * - `{{%district}}`
 * - `{{%street}}`
 * - `{{%house}}`
 * - `{{%users}}`
 * - `{{%users}}`
 */
class m200120_202546_create_applications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%applications}}', [
            'id' => $this->primaryKey(),
            'date_cr' => $this->datetime()->comment('Дата и время создание'),
            'city_id' => $this->integer()->comment('Город'),
            'district_id' => $this->integer()->comment('Район'),
            'street_id' => $this->integer()->comment('Улица'),
            'house_id' => $this->integer()->comment('Дом'),
            'kvartira' => $this->string(255)->comment('Квартира'),
            'l_s' => $this->string(255)->comment('Л\С'),
            'fio' => $this->string(255)->comment('ФИО'),
            'phone' => $this->string(255)->comment('Телефон'),
            'description' => $this->text()->comment('Описание'),
            'status' => $this->integer()->comment('Статус'),
            'creator_id' => $this->integer()->comment('Создатель'),
            'comment' => $this->text()->comment('Комментария'),
            'date_end' => $this->date()->comment('Дата завершение'),
            'executor_id' => $this->integer()->comment('Исполнитель'),
            'date_execute' => $this->date()->comment('Дата исполнения'),
            'comment_execute' => $this->text()->comment('Отчет выполнения'),
        ]);

        // creates index for column `city_id`
        $this->createIndex(
            '{{%idx-applications-city_id}}',
            '{{%applications}}',
            'city_id'
        );

        // add foreign key for table `{{%city}}`
        $this->addForeignKey(
            '{{%fk-applications-city_id}}',
            '{{%applications}}',
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE'
        );

        // creates index for column `district_id`
        $this->createIndex(
            '{{%idx-applications-district_id}}',
            '{{%applications}}',
            'district_id'
        );

        // add foreign key for table `{{%district}}`
        $this->addForeignKey(
            '{{%fk-applications-district_id}}',
            '{{%applications}}',
            'district_id',
            '{{%district}}',
            'id',
            'CASCADE'
        );

        // creates index for column `street_id`
        $this->createIndex(
            '{{%idx-applications-street_id}}',
            '{{%applications}}',
            'street_id'
        );

        // add foreign key for table `{{%street}}`
        $this->addForeignKey(
            '{{%fk-applications-street_id}}',
            '{{%applications}}',
            'street_id',
            '{{%street}}',
            'id',
            'CASCADE'
        );

        // creates index for column `house_id`
        $this->createIndex(
            '{{%idx-applications-house_id}}',
            '{{%applications}}',
            'house_id'
        );

        // add foreign key for table `{{%house}}`
        $this->addForeignKey(
            '{{%fk-applications-house_id}}',
            '{{%applications}}',
            'house_id',
            '{{%house}}',
            'id',
            'CASCADE'
        );

        // creates index for column `creator_id`
        $this->createIndex(
            '{{%idx-applications-creator_id}}',
            '{{%applications}}',
            'creator_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-applications-creator_id}}',
            '{{%applications}}',
            'creator_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );

        // creates index for column `executor_id`
        $this->createIndex(
            '{{%idx-applications-executor_id}}',
            '{{%applications}}',
            'executor_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-applications-executor_id}}',
            '{{%applications}}',
            'executor_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%city}}`
        $this->dropForeignKey(
            '{{%fk-applications-city_id}}',
            '{{%applications}}'
        );

        // drops index for column `city_id`
        $this->dropIndex(
            '{{%idx-applications-city_id}}',
            '{{%applications}}'
        );

        // drops foreign key for table `{{%district}}`
        $this->dropForeignKey(
            '{{%fk-applications-district_id}}',
            '{{%applications}}'
        );

        // drops index for column `district_id`
        $this->dropIndex(
            '{{%idx-applications-district_id}}',
            '{{%applications}}'
        );

        // drops foreign key for table `{{%street}}`
        $this->dropForeignKey(
            '{{%fk-applications-street_id}}',
            '{{%applications}}'
        );

        // drops index for column `street_id`
        $this->dropIndex(
            '{{%idx-applications-street_id}}',
            '{{%applications}}'
        );

        // drops foreign key for table `{{%house}}`
        $this->dropForeignKey(
            '{{%fk-applications-house_id}}',
            '{{%applications}}'
        );

        // drops index for column `house_id`
        $this->dropIndex(
            '{{%idx-applications-house_id}}',
            '{{%applications}}'
        );

        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-applications-creator_id}}',
            '{{%applications}}'
        );

        // drops index for column `creator_id`
        $this->dropIndex(
            '{{%idx-applications-creator_id}}',
            '{{%applications}}'
        );

        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-applications-executor_id}}',
            '{{%applications}}'
        );

        // drops index for column `executor_id`
        $this->dropIndex(
            '{{%idx-applications-executor_id}}',
            '{{%applications}}'
        );

        $this->dropTable('{{%applications}}');
    }
}
