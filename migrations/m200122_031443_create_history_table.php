<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%history}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%users}}`
 */
class m200122_031443_create_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%history}}', [
            'id' => $this->primaryKey(),
            'table_name' => $this->string(255)->comment('Таблица'),
            'line_id' => $this->integer()->comment('ИД'),
            'date_time' => $this->datetime()->comment('Дата и время'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'user_fio' => $this->string(255)->comment('ФИО пользователя'),
            'field' => $this->string(255)->comment('Поля'),
            'old_value' => $this->text()->comment('Значение'),
            'new_value' => $this->text()->comment('Изменение'),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-history-user_id}}',
            '{{%history}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-history-user_id}}',
            '{{%history}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-history-user_id}}',
            '{{%history}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-history-user_id}}',
            '{{%history}}'
        );

        $this->dropTable('{{%history}}');
    }
}
