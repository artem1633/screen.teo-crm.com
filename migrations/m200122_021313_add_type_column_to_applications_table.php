<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%applications}}`.
 */
class m200122_021313_add_type_column_to_applications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%applications}}', 'type', $this->integer()->comment('Тип'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%applications}}', 'type');
    }
}
