<?php

use yii\db\Migration;

/**
 * Class m200121_180300_add_foreignKey_to_application_table
 */
class m200121_180300_add_foreignKey_to_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // creates index for column `status`
        $this->createIndex(
            '{{%idx-applications-status}}',
            '{{%applications}}',
            'status'
        );

        // add foreign key for table `{{%application_status}}`
        $this->addForeignKey(
            '{{%fk-applications-status}}',
            '{{%applications}}',
            'status',
            '{{%application_status}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%application_status}}`
        $this->dropForeignKey(
            '{{%fk-applications-status}}',
            '{{%applications}}'
        );

        // drops index for column `status`
        $this->dropIndex(
            '{{%idx-applications-status}}',
            '{{%applications}}'
        );
    }
}
