<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%district}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%city}}`
 */
class m200120_190001_create_district_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%district}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'city_id' => $this->integer()->comment('Город'),
        ]);

        // creates index for column `city_id`
        $this->createIndex(
            '{{%idx-district-city_id}}',
            '{{%district}}',
            'city_id'
        );

        // add foreign key for table `{{%city}}`
        $this->addForeignKey(
            '{{%fk-district-city_id}}',
            '{{%district}}',
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE'
        );

        $this->insert('district',array(
            'name' => 'Московская',
            'city_id' => 1,
        ));

        $this->insert('district',array(
            'name' => 'Келес',
            'city_id' => 2,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%city}}`
        $this->dropForeignKey(
            '{{%fk-district-city_id}}',
            '{{%district}}'
        );

        // drops index for column `city_id`
        $this->dropIndex(
            '{{%idx-district-city_id}}',
            '{{%district}}'
        );

        $this->dropTable('{{%district}}');
    }
}
