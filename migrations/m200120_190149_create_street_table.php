<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%street}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%district}}`
 */
class m200120_190149_create_street_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%street}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'district_id' => $this->integer()->comment('Город'),
        ]);

        // creates index for column `district_id`
        $this->createIndex(
            '{{%idx-street-district_id}}',
            '{{%street}}',
            'district_id'
        );

        // add foreign key for table `{{%district}}`
        $this->addForeignKey(
            '{{%fk-street-district_id}}',
            '{{%street}}',
            'district_id',
            '{{%district}}',
            'id',
            'CASCADE'
        );

        $this->insert('street',array(
            'name' => 'Улянова',
            'district_id' => 1,
        ));

        $this->insert('street',array(
            'name' => 'Тарасова',
            'district_id' => 2,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%district}}`
        $this->dropForeignKey(
            '{{%fk-street-district_id}}',
            '{{%street}}'
        );

        // drops index for column `district_id`
        $this->dropIndex(
            '{{%idx-street-district_id}}',
            '{{%street}}'
        );

        $this->dropTable('{{%street}}');
    }
}
