<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180601_154502_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('ФИО'),//required
            'login' => $this->string(255)->unique()->comment('Логин/Е-mail'),//required
            'password' => $this->string(255)->comment('Пароль'),//required
            'telephone' => $this->string(255)->comment('Телефон'),
            'role_id' => $this->integer(255)->comment('Должность'),
            'auth_key' => $this->string(255)->comment('Пароль'),
        ]);

        $this->insert('users',array(
            'name' => 'Иванов Иван Иванович',
            'login' => 'admin',
            'password' => md5('admin'),
            'telephone' => '+7 961 123 45 67',
            'role_id' => 1, //администратор
            'auth_key' => 'admin',
        ));

        $this->insert('users',array(
            'name' => 'Исполнитель',
            'login' => 'user',
            'password' => md5('user'),
            'telephone' => '+7 961 111 11 1',
            'role_id' => 3, //user
            'auth_key' => 'user',
        ));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
