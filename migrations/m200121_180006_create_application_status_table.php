<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%application_status}}`.
 */
class m200121_180006_create_application_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%application_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Название'),
            'color' => $this->string(255)->comment('Цвет'),
        ]);

        $this->insert('application_status',array(
            'name' => 'Новая',
            'color' => '#80ffff',
        ));

        $this->insert('application_status',array(
            'name' => 'В работе',
            'color' => '#ffff00',
        ));

        $this->insert('application_status',array(
            'name' => 'Завершено',
            'color' => '#00ff00',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%application_status}}');
    }
}
