<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Applications;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
        'checkboxOptions' => function($model) {
            if(Yii::$app->user->identity->role_id != 1){
               return ['disabled' => true];
            }else{
               return [];
            }
         },
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_cr',
        'filterType' => GridView::FILTER_DATE_RANGE,
        'format' =>  ['date', 'Y-MM-dd HH:mm' ],
        'filterWidgetOptions'=>[
            'pluginOptions'=>[
               "timePicker"=> true,
                "timePicker24Hour"=> true,
                'locale' => [
                    'format' => 'YYYY-MM-DD HH:mm', 
                ]
            ],
        ],
        'content' => function($data){
            if($data->date_cr != null) return date('d.m.Y H:i', strtotime($data->date_cr) );
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'city_id',
        'filter' => Applications::getCityList(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => 'Выберите'],
            'pluginOptions' => [
                'allowClear' => true,
                'width'=>'130px'
            ],
        ],
        'content' => function($data){
            return $data->city->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'district_id',
        'filter' => Applications::getDistrictList(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => 'Выберите'],
            'pluginOptions' => [
                'allowClear' => true,
                'width'=>'130px'
            ],
        ],
        'content' => function($data){
            return $data->district->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'street_id',
        'filter' => Applications::getStreetList(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => 'Выберите'],
            'pluginOptions' => [
                'allowClear' => true,
                'width'=>'130px'
            ],
        ],
        'content' => function($data){
            return $data->street->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'house_id',
        'filter' => Applications::getHouseList(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => 'Выберите'],
            'pluginOptions' => [
                'allowClear' => true,
                'width'=>'130px'
            ],
        ],
        'content' => function($data){
            return $data->house->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'kvartira',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'l_s',
    // ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'filter' => Applications::getTypeList(),
        'content' => function($data){
            return $data->getTypeList()[$data->type];
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
        'content'=>function($data){
            if($data->description != null) return mb_substr($data->description, 0, 60) . "...";
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'filter' => Applications::getStatusList(),
        'content' => function($data){
            return $data->getStatusList()[$data->status];
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'creator_id',
        'filter' => Applications::getCreatorList(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => 'Выберите'],
            'pluginOptions' => [
                'allowClear' => true,
                'width'=>'130px'
            ],
        ],
        'content' => function($data){
            return $data->creator->name;
        }
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'comment',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'date_end',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'executor_id',
        'filter' => Applications::getExecutorList(),
        'filterType' => GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'options' => ['prompt' => 'Выберите'],
            'pluginOptions' => [
                'allowClear' => true,
                'width'=>'130px'
            ],
        ],
        'content' => function($data){
            return $data->executor->name;
        }
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'date_execute',
    // ],
        /*[
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{update} {delete}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['data-pjax'=>'0','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'], 
    ],*/
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{update} {leadDelete}',
        'buttons'  => [
            'leadDelete' => function ($url, $model) {
                if(Yii::$app->user->identity->role_id == 1){
                    $url = Url::to(['/users/delete', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'role'=>'modal-remote','title'=>'Удалить', 
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ]);
                }
            },
        ],
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
    ],

];   