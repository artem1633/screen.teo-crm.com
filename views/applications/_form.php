<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Applications */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="box box-warning box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">Создание/Изменение</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="applications-form" style="padding: 0px 20px 0px 20px;">

            <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'city_id')->widget(kartik\select2\Select2::classname(), [
                        'data' => $model->getCityList(),
                        'options' => [
                            'placeholder' => 'Выберите',
                            'disabled' => $model->getDisabled('city_id'),
                            'onchange'=>'
                                $.post( "/applications/districts?id='.'"+$(this).val(), function( data ){
                                    $( "select#applications-district_id" ).html( data);
                                });' 
                        ],
                        'size' => kartik\select2\Select2::SIZE_MEDIUM ,
                        'pluginOptions' => [
                            'multiple' => false,
                            'allowClear' => true,
                        ],
                    ])?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'district_id')->widget(kartik\select2\Select2::classname(), [
                        'data' => $model->getDistrictList($model->city_id, 1),
                        'options' => [
                            'placeholder' => 'Выберите',
                            'disabled' => $model->getDisabled('district_id'),
                            'onchange'=>'
                                $.post( "/applications/streets?id='.'"+$(this).val(), function( data ){
                                    $( "select#applications-street_id" ).html( data);
                                });' 
                        ],
                        'size' => kartik\select2\Select2::SIZE_MEDIUM ,
                        'pluginOptions' => [
                            'multiple' => false,
                            'allowClear' => true,
                        ],
                    ])?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'street_id')->widget(kartik\select2\Select2::classname(), [
                        'data' => $model->getStreetList($model->district_id, 1),
                        'options' => [
                            'placeholder' => 'Выберите',
                            'disabled' => $model->getDisabled('street_id'),
                            'onchange'=>'
                                $.post( "/applications/house?id='.'"+$(this).val(), function( data ){
                                    $( "select#applications-house_id" ).html( data);
                                });' 
                        ],
                        'size' => kartik\select2\Select2::SIZE_MEDIUM ,
                        'pluginOptions' => [
                            'multiple' => false,
                            'allowClear' => true,
                        ],
                    ])?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'house_id')->widget(kartik\select2\Select2::classname(), [
                        'data' => $model->getHouseList($model->street_id, 1),
                        'options' => ['placeholder' => 'Выберите ...', 'disabled' => $model->getDisabled('house_id')],
                        'size' => kartik\select2\Select2::SIZE_MEDIUM ,
                        'pluginOptions' => [
                            'multiple' => false,
                            'allowClear' => true,
                        ],
                    ])?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'kvartira')->textInput(['maxlength' => true, 'disabled' => $model->getDisabled('kvartira')]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'l_s')->textInput(['maxlength' => true, 'disabled' => $model->getDisabled('l_s')]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'fio')->textInput(['maxlength' => true, 'disabled' => $model->getDisabled('fio')]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'disabled' => $model->getDisabled('phone')]) ?>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($model, 'type')->dropDownList($model->getTypeList(),['disabled' => $model->getDisabled('type')]) ?>
                </div>
                <div class="col-md-5">
                    <?= $form->field($model, 'description')->textarea(['rows' => 3, 'disabled' => $model->getDisabled('description')]) ?>
                </div>
                <div class="col-md-5">
                    <?= $form->field($model, 'comment')->textarea(['rows' => 3, 'disabled' => $model->getDisabled('comment')]) ?>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'status')->dropDownList($model->getStatusList(),['disabled' => $model->getDisabled('status')]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'executor_id')->widget(kartik\select2\Select2::classname(), [
                        'data' => $model->getExecutorList(),
                        'options' => ['placeholder' => 'Выберите ...', 'disabled' => $model->getDisabled('executor_id')],
                        'size' => kartik\select2\Select2::SIZE_MEDIUM ,
                        'pluginOptions' => [
                            'multiple' => false,
                            'allowClear' => true,
                        ],
                    ])?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'date_execute')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'Выберите', 'disabled' => $model->getDisabled('date_execute')],
                            'layout' => '{picker}{input}',
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd',
                                //'startView'=>'year',
                            ]
                        ]) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'comment_execute')->textarea(['rows' => 2, 'disabled' => $model->getDisabled('comment_execute')]) ?>
                </div>
            </div>
          
            <?php if (!Yii::$app->request->isAjax){ ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Назад', ['index'], ['data-pjax'=>'0','title'=> 'Назад','class'=>'btn btn-warning']) ?>
                    <?php if(!$model->isNewRecord) { ?>
                        <?= Html::a('История', ['history', 'id' => $model->id], ['data-pjax'=>'0','title'=> 'История','class'=>'btn btn-success']) ?>
                    <?php } ?>
                </div>
            <?php } ?>

            <?php ActiveForm::end(); ?>
            
        </div>
    </div>
</div>


