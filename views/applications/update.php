<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Applications */
$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="applications-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
