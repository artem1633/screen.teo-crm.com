<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Users;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'filter' => ArrayHelper::map(Users::find()->all(),'id','name'),
        'content' => function($data){
            $user = Users::findOne($data->user_id);
            if($user !== null) return $user->name;
            else return $data->user_fio;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_time',
        'content' => function($data){
            return date( 'H:i d.m.Y', strtotime($data->date_time));
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'field',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'old_value',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'new_value',
    ],
];   