<?php
use yii\helpers\Html;
use kartik\grid\GridView;
?>

<div class="box box-warning box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">Создание/Изменение</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="applications-form" style="padding: 0px 20px 0px 20px;">
            <div class="row">
                <div class="col-md-12">
                    <?= $this->render('search', [
                        'post' => $post,
                    ]) ?> 
                </div>
                <div class="col-md-12">
                    <br>
                    <?=GridView::widget([
                        'id'=>'history',
                        'dataProvider' => $historyProvider,
                        //'filterModel' => $searchHistory,
                        'pjax'=>true,
                        'columns' => require(__DIR__.'/history_columns.php'),
                        'toolbar'=> [
                            ['content'=>
                                    '
                                        <ul class="panel-controls">
                                            <li>{export}</li>
                                        </ul>'
                            ],
                        ],           
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'type' => 'info', 
                            'heading' => '<i class="glyphicon glyphicon-list"></i> История изменений',
                            'before'=>'',
                            'after'=>'',
                            ]
                    ])?>
                    <?= Html::a('В таблицу', ['index'], ['data-pjax'=>'0','title'=> 'Назад','class'=>'btn btn-warning']) ?>
                </div>
            </div>
        </div>
    </div>
</div>


