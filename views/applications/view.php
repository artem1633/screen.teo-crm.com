<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Applications */
?>
<div class="applications-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date_cr',
            'city',
            'district_id',
            'street_id',
            'house_id',
            'kvartira',
            'l_s',
            'fio',
            'phone',
            'description:ntext',
            'status',
            'creator_id',
            'comment:ntext',
            'date_end',
            'executor_id',
            'date_execute',
        ],
    ]) ?>

</div>
