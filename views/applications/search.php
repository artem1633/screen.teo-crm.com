<?php
use yii\helpers\Html;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use app\models\Applications;

$this->title = 'История';
$this->params['breadcrumbs'][] = $this->title;

?>
<?php 
$layout = <<< HTML
    <span class="input-group-addon label-success">С</span>
    {input1}
    <span class="input-group-addon label-success">По</span>
    {input2}
    <span class="input-group-addon kv-date-remove">
        <i class="glyphicon glyphicon-remove"></i>
    </span>
HTML;
?>

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-5"> 
            <label>Интервал</label><br>
            <?= DatePicker::widget([
                'type' => DatePicker::TYPE_RANGE,
                'name' => 'date_time_from',
                'value' => $post['date_time_from'],
                'name2' => 'date_time_to',
                'value2' => $post['date_time_to'],
                'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                'layout' => $layout,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',
                ]
            ]);
            ?>
        </div>
        <div class="col-md-5">
            <label>Поля</label><br>
            <?= kartik\select2\Select2::widget([
                'name' => 'field',
                'data' => Applications::getFieldList(),
                'value' => $post['field'],
                'size' => 'sm',
                'options' => [ 'placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'multiple' => true,
                    'allowClear' => true,
                ],
            ])
            ?>
        </div>
        <div class="col-md-1">
            <label>.</label><br>
            <div class="form-group" >
                <?= Html::submitButton('Поиск', ['class' =>  'btn btn-info']) ?>
            </div>
        </div>
     </div>   
<?php ActiveForm::end(); ?>