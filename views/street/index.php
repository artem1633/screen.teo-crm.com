<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AtelierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Улица к району';
$this->params['breadcrumbs'][] = $this->title;
$role_id = Yii::$app->user->identity->role_id;

CrudAsset::register($this);

?>
<div class="atelier-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    $role_id == 1 ? Html::a('<i class="fa fa-plus"></i>', ['create'],
                        ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-primary']) . '{export}' : '' . '{export}'/*.'{toggleData}'*/
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,          
            'panel' => [
                'type' => 'primary', 
                'heading' => '<i class="glyphicon glyphicon-list"></i> Список',
                'before'=>'',
                'after'=>BulkButtonWidget::widget([
                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить все',
                        ["bulk-delete"] ,
                        [
                            "class"=>"btn btn-danger btn-xs",
                            'role'=>'modal-remote-bulk',
                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                            'data-request-method'=>'post',
                            'data-confirm-title'=>'Подтвердите действие',
                            'data-confirm-message'=>'Вы уверены что хотите удалить этих элементов?'
                        ]),
                ]).                        
                '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
