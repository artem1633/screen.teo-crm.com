<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Street */
?>
<div class="street-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'district_id',
        ],
    ]) ?>

</div>
