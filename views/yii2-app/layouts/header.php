<?php
use yii\helpers\Html;
use yii\helpers\Url;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\bootstrap\Modal;

CrudAsset::register($this);
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini"><img src="http://' . $_SERVER['SERVER_NAME'] .'/images/logo.png"/></span><span class="logo-lg"><img src="http://' . $_SERVER['SERVER_NAME'] .'/images/logo.png"/></span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" onclick="$.post('/site/menu-position');" class="sidebar-toggle" data-toggle="push-menu" role="button"><span class="sr-only">Toggle navigation</span> </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><?=Yii::$app->user->identity->name ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="<?= 'http://' . $_SERVER['SERVER_NAME'] ?>/images/nouser.png" class="img-circle" alt="User Image"/>
                            <p> <?=Yii::$app->user->identity->name ?> </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a('Изменить пароль', ['users/change', 'id' => Yii::$app->user->identity->id],
                                ['role'=>'modal-remote','title'=> 'Изменить пароль','class'=>'btn btn-default btn-flat']); ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Выход',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
                
            </ul>
        </div>
    </nav>
</header>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>