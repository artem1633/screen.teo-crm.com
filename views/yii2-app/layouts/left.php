<?php

use yii\helpers\Html;
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <?php 
        if(isset(Yii::$app->user->identity->id)) { 
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                        'items' => [
                            //['label' => 'Menu', 'options' => ['class' => 'header']],
                            ['label' => 'Заявки', 'icon' => 'cube', 'url' => ['/applications']],
                            ['label' => 'Отчеты', 'icon' => 'bar-chart-o', 'url' => ['/city/report']],
                            ['label' => 'Профиль', 'icon' => 'user', 'url' => ['/city/profile']],
                            [
                                'label' => 'Настройки',
                                'icon' => 'book',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Пользователи', 'icon' => 'file-text-o', 'url' => ['/users']],
                                    ['label' => 'Статусы заявок', 'icon' => 'file-text-o', 'url' => ['/application-status']],
                                    [
                                        'label' => 'Журнал адресов',
                                        'icon' => 'book',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Город', 'icon' => 'file-text-o', 'url' => ['/city'],],
                                            ['label' => 'Район', 'icon' => 'file-text-o', 'url' => ['/district'],],
                                            ['label' => 'Улица', 'icon' => 'file-text-o', 'url' => ['/street'],],
                                            ['label' => 'Дома', 'icon' => 'file-text-o', 'url' => ['/house'],],
                                        ],
                                    ],
                                ],
                            ],
                            ['label' => 'Выход', 'icon' => 'sign-out', 'url' => ['/site/logout']],
                        ],
                    ]
                );
            } 
            else{
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                        'items' => [
                            ['label' => 'Menu', 'options' => ['class' => 'header']],
                            ['label' => 'Signup', 'icon' => 'sign-in', 'url' => ['/signup']],                            
                        ],
                    ]
                );
            }
        ?>
    </section>
</aside>