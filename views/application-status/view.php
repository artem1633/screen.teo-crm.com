<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApplicationStatus */
?>
<div class="application-status-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'color',
        ],
    ]) ?>

</div>
