<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ApplicationStatus */
?>
<div class="application-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
