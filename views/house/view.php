<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\House */
?>
<div class="house-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'street_id',
        ],
    ]) ?>

</div>
