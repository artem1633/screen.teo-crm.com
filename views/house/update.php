<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\House */
?>
<div class="house-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
