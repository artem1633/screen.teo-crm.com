<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

$this->title = 'Заголовок';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box box-warning box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">Заголовок</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="applications-form" style="padding: 0px 20px 0px 20px;">
            <div class="row">
                <div class="col-md-12">
                    <div style="height: 200px; color:red; text-align: center; font-size: 24px; vertical-align: middle;">
                        <br>
                        <br>
                        Страница в разработке
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>